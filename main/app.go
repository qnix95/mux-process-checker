package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", hello)
}

func hello(w http.ResponseWriter, r *http.Request){
	var id = os.Getpid()
	var _ = fibo(12)
	_, _ = fmt.Fprintf(w, "%d", id)
}

func fibo(n int) int {
	if n <= 1 {
		return n
	} else {
		return fibo(n-2) + fibo(n-1)
	}
}